# TinyTiny RSS Docker

## Description
This project runs the actual git master code for TinyTinyRSS inside a Debian Buster container with PHP. This container uses the built-in php server to provide the html and php files on port `8080`.

### Entrypoint
The `entrypoint.sh` script checks if the database is ready for connections and if the database is already created. This checks are important for the correct startup of the service and to prevent the database to be reseted when the container restarts.

## Usage
- Clone this repo **or** just copy the `docker-compose.yml` and the `.env.example` as `.env` into a folder
- Set the needed environment values
- Run `docker-compose up -d`

### Environment / Configuration
#### Authentication
`TTRSS_SINGLE_USER` (**Default** `false`): If this is `true` no authentification is requiered. Login and registration are deactivated.  
`TTRSS_SIMPLE_UPDATE_MODE` (**Default** `false`): If this is `true` ttrss tries to update the news feed when the browser is open and visiting the site. This is not needed because a cron job is active inside the container.  
`TTRSS_SELF_REGISTRATION` (**Default** `false`): If this is `true` visitors can create own accounts
`TTRSS_MAX_USERS` (**Default** `1`): How many users can be registered on this instance. If self registration is `false` the maximal number is `1`, if it is `true` the number from this envrionment variable is used.

#### Database
`TTRSS_DATABASE_TYPE` (**Default** `pgsql`): The type of database which is used. Other value `mysql`  
`TTRSS_DATABASE_HOST` (**Default** `database`): The address of the database. Here docker-compose service name.  
`TTRSS_DATABASE_PORT` (**Default** `5432`): Port of the database. For mysql use `3306`  

`POSTGRES_USER` (**Default** `user`): User for the postgres database  
`POSTGRES_PASSWORD` (**Default** `password`): Password for the database user  
`POSTGRES_DB` (**Default** `ttrss`): The database name that should be used  

#### E-Mail
`TTRSS_SMTP_FROM_NAME` (**Default** `Tiny Tiny RSS`): The name that is displayed as email sender  
`TTRSS_SMTP_ADDRESS` (**Default** `no-reply@example.org`): The address that is displayed as email sender  
`TTRSS_SMTP_SERVER` (**Default** `smtp.example.org:25`): The smtp address of your email provider  
`TTRSS_SMTP_USER` (**Default** `no-reply@example.org`): The user that is used for sending emails
`TTRSS_SMTP_PASSWORD` (**Default** `Password`): The password for the smtp user  
`TTRSS_SMTP_SECURE_METHOD` (**Default** ` `): Possible values: `ssl` or `tls`  

#### Web
`TTRSS_URI` (**Default** `http://localhost/`): The url from which this service is reachable