FROM marvin21/ttrss-base:latest

LABEL maintainer "Marvin Dalheimer <me@marvin-dalheimer.de>"

ADD ./tt-rss /var/www/html
ADD ./config.php /var/www/html/config.php

ADD ./entrypoint.sh /bin/entrypoint.sh
RUN chmod +x /bin/entrypoint.sh

ADD ./feed-update.cron /etc/cron.d/feed-update
RUN chmod 0644 /etc/cron.d/feed-update && \
    crontab /etc/cron.d/feed-update

RUN useradd -ms /bin/bash ttrss
RUN chown -R ttrss /var/www/html

USER ttrss
WORKDIR /var/www/html

EXPOSE 80
CMD ["entrypoint.sh"]
