#!/bin/bash
set -e

export PGPASSWORD=$TTRSS_DATABASE_PASSWORD

until psql -h "$TTRSS_DATABASE_HOST" -p "$TTRSS_DATABASE_PORT" -U "$TTRSS_DATABASE_USER" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 1
done

>&2 echo "Postgres is up - executing command"

if [[ $(psql -h "$TTRSS_DATABASE_HOST" -p "$TTRSS_DATABASE_PORT" -U "$TTRSS_DATABASE_USER" -tAc "SELECT EXISTS (SELECT 1 FROM information_schema.tables WHERE table_name = 'ttrss_feeds');") = 't' ]]; then
  echo "Database already initialized"
else
  echo "Import database dump"
  psql -h "$TTRSS_DATABASE_HOST" -p "$TTRSS_DATABASE_PORT" -U "$TTRSS_DATABASE_USER" "$TTRSS_DATABASE_NAME" < /var/www/html/schema/ttrss_schema_pgsql.sql
fi

echo "Start server"
php -S 0.0.0.0:8080 -t /var/www/html
